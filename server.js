require('dotenv').config();
const http = require('http');
const fs = require('fs');
const {v4: uuidv4} = require('uuid');
const {translateText, getAllTranslationLanguages} = require('./lib/azure-translator')
const clients = new Map();

const ws = new require('ws');

const wss = new ws.Server({noServer: true});

async function accept(req, res) {
  if (req.url === '/ws' && req.headers.upgrade &&
    req.headers.upgrade.toLowerCase() === 'websocket' &&
    // can be Connection: keep-alive, Upgrade
    req.headers.connection.match(/\bupgrade\b/i)) {
    wss.handleUpgrade(req, req.socket, Buffer.alloc(0), onSocketConnect);
  } else if (req.url === '/') {
    fs.createReadStream('./index.html').pipe(res);
  } else if (req.url === '/languages') {
    res.writeHead(200, {'Content-Type': 'application/json'})
    res.end(JSON.stringify({result: await getAllTranslationLanguages()}));
  } else { // page not found
    res.writeHead(404);
    res.end();
  }
}

function onSocketConnect(ws) {
  ws.id = uuidv4()

  ws.on('message', async (receivedMessage) => {
    const parsedMessage = JSON.parse(receivedMessage);
    let messageToSend;

    if (parsedMessage?.user) {
      const {username, locale} = parsedMessage.user
      clients.set(ws.id, {locale, username, connection: ws});
    } else {
      const formattedMessage = parsedMessage.message.slice(0, 180); // max message length will be 180
      const {username} = clients.get(ws.id);

      for (let [, clientInfo] of clients) {
        messageToSend = {
          username,
          message: await translateText(formattedMessage, clientInfo.locale)
        };
        clientInfo.connection.send(JSON.stringify(messageToSend));
      }
    }
  })

  ws.on('close', function (e) {
    clients.delete(ws.id);
  });
}

http.createServer(accept).listen(8080);
