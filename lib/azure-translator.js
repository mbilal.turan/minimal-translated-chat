/* This simple app uses the '/translate' resource to translate text from
one language to another. */

/* This template relies on the request module, a simplified and user friendly
way to make HTTP requests. */
const axios = require('axios');
const {v4: uuidv4} = require('uuid');

const key_var = 'TRANSLATOR_TEXT_SUBSCRIPTION_KEY';
if (!process.env[key_var]) {
  throw new Error('Please set/export the following environment variable: ' + key_var);
}
const subscriptionKey = process.env[key_var];
const endpoint_var = 'TRANSLATOR_TEXT_ENDPOINT';
if (!process.env[endpoint_var]) {
  throw new Error('Please set/export the following environment variable: ' + endpoint_var);
}
const endpoint = process.env[endpoint_var];
const region_var = 'TRANSLATOR_TEXT_REGION_AKA_LOCATION';
if (!process.env[region_var]) {
  throw new Error('Please set/export the following environment variable: ' + region_var);
}
const region = process.env[region_var];

module.exports = {
  async translateText(text, locale) {
    let options = {
      method: 'POST',
      baseURL: endpoint,
      url: '/translate',
      params: {
        'api-version': '3.0',
        'to': locale
      },
      headers: {
        'Ocp-Apim-Subscription-Key': subscriptionKey,
        'Ocp-Apim-Subscription-Region': region,
        'Content-type': 'application/json',
        'X-ClientTraceId': uuidv4().toString()
      },
      data: [{
        text
      }],
      json: true,
    };

    try {
      const result = await axios(options);
      return result.data[0].translations[0].text;
    } catch(e) {
      console.error(e.response.data)
      console.error(e.message)
    }

  },

  async getAllTranslationLanguages() {
    let options = {
      method: 'GET',
      baseURL: endpoint,
      url: '/languages',
      params: {
        'api-version': '3.0',
        'scope': 'translation'
      },
      headers: {
        'Content-type': 'application/json',
        'X-ClientTraceId': uuidv4().toString()
      },
      json: true,
    };

    const result = await axios(options);

    return Object.entries(result.data.translation).reduce((acc, [locale, info]) => [
      ...acc,
      {
        name: info.name,
        locale
      }], [])
  }

};
