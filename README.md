# prereqs
- nodejs >=v14


# install & prep
- run `npm install` to install dependencies.

- set up an `.env` file using .env.example file as a reference.
you can follow the instructions here to get your subscription key:
https://docs.microsoft.com/en-us/azure/cognitive-services/translator/translator-how-to-signup#get-your-authentication-keys-and-endpoint

# usage
- run the app with `npm start`
- access the app on: http://localhost:8080
